//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  ufrmbase.pas     
//
//  创建: changym by 2011-11-19
//        changup@qq.com                    
//  新增功能:
//      完成窗体字体设置;
//      公用函数;
//      FormClose事件：Action := caFree;
//      FormKeyPress事件：将回车映射为tab键;
//
//  修改历史:
//                                            
//  版权所有 (C) 2011 by changym
//                                                       
//*******************************************************************

unit ufrmbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, HzSpell, Wwdbgrid, uglobal, dateUtils,
  ubase, UpSpeedButton, upedit, Wwdbigrd, UpCnedit, UpCheckBox, UpCheckListBox,
  UpComboBox, UpDateTimePicker, UpListBox, UpMemo, Grids, CnSpin, CnEdit,
  UpRadioButton, Menus, UpPopupMenu, utypes, UPanel, DB, ADODB, UpWWDbGrid,
  UpPageControl,ComCtrls, umysystem, wwdblook, wwExport,AdvDateTimePicker
  {$ifndef ISAPP}
  ,udllfuns_dbgridSelfDraw, ExtCtrls;
  {$ELSE}
  ;
  {$ENDIF}

function TUpSpeedButtonCompareLeft(Item1, Item2: Pointer): Integer;
function TUpSpeedButtonCompareRight(Item1, Item2: Pointer): Integer;

type
  Tfrmbase = class(TForm)
    pmkuaijcd: TUpPopupMenu;
    pmnlisjiy: TMenuItem;
    pmnbaocls: TMenuItem;
    pmnhuifls: TMenuItem;
    mniqingc: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure pmnbaoclsClick(Sender: TObject);
    procedure pmnhuiflsClick(Sender: TObject);
    procedure mniqingcClick(Sender: TObject);
  private
    procedure upmessageproc(var msg : TMSG); message UPM_PANEL_RESIZE;
    //父亲大小改变通知消息;具体参数请见消息定义说明;
    //2012-06-22:
    //目前用于将窗体装进TPanel中时,在panel.resize事件中广播此消息以通知
    //他当容器的子窗体大小自适应改变;

  private
    procedure DbgridOnCalcCellColor(Sender: TObject; Field: TField;
        State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    //dbgrid自画cell颜色

  protected
    procedure alignleft_buttons(); virtual;
    //TUpSpeedButton.alignleft属性=true的按钮左对齐

    procedure alignright_buttons(); virtual;
    //TUpSpeedButton.alignright属性=true的按钮右对齐
  public
    errcode : Integer;
    //错误代码
    errmsg : string;
    //错误描述

    baseobj : tbase;
    //基础类对象

  public
    sourceformhandle : THandle;
    //来源窗体句柄  
    procedure nitified_sourceform();
    //通知来源窗体消息
  protected
    procedure upmsg_desform_notified(var msg : TMSG); message UPM_DESFORM2SOURCEFORM_NOTIFIED;
    //消息响应函数;
    procedure upmsg_desform_notifiedmsg_process(); virtual;
    //来自于目标窗体的消息处理函数;

  public
    procedure SaveHistory();
    //控件存储历史值
    procedure LoadHistory();
    //加载控件历史值
  end;

implementation

{$R *.dfm}

procedure Tfrmbase.FormClose(Sender: TObject; var Action: TCloseAction);
var
  frmhandle : THandle;
  p : TWinControl;
begin
  baseobj.Free;
  baseobj := nil;

  //当作为TPanel的儿子窗体出现的时候,在关闭的时候发送消息给TPanel所在的窗体,
  //自己关闭的消息, 暂时2012-7-31：用于调整TPanel所在窗体导航栏显示
  frmhandle := 0;
  if (Self.Parent <> nil) and (Self.Parent.InheritsFrom(TUPanel)) then
  begin
    p := Self.Parent;
    while (frmhandle = 0) and (p <> nil) do
    begin
      if p is TForm then
        frmhandle := p.Handle
      else
        p := p.Parent;
    end;
    if frmhandle <> 0 then
      SendMessage(Self.Parent.Parent.handle, UPM_PANEL_SUBFORM_ONCLOSE, 0, 0);
  end;

  Action := caFree;
end;

procedure Tfrmbase.FormKeyPress(Sender: TObject; var Key: Char);
begin
  { 处理回车模仿tab键在控件间跳转  }

  //检查回车是否按下
  if Key = #13 then
  begin
    { 2012-3-20
      当前活动控件是TwwDBGrid,在字段间跳转焦点
    }
    { 2012-5-28
      TwwDBGrid的按键消息由自己处理
    }
    if ActiveControl is TUpWWDbGrid then
    begin
      { TwwDBGrid的焦点转移由TwwDBGrid自己的属性解决
      with TUpWWDbGrid(ActiveControl) do
      begin
        if SelectedIndex < (FieldCount-1) then
        begin
          SelectedIndex := SelectedIndex + 1;
        end
        else
        begin
          SelectedIndex := 0;
        end;
      end;
      }
    end     
    { TUpEdit有时候要处理回车键, 不能响应这里的预处理 }
    else if ActiveControl is TUpEdit then
    begin
      if (ActiveControl as TUpEdit).BKeyPreview = False then Exit
      else
      begin
        { 未特殊处理, 将回车模拟成tab键 }
        //吃掉回车键
        Key := #0;

        //发送焦点转移消息
        Perform(WM_NEXTDLGCTL, 0, 0);
      end;
    end
    { TUpCnEdit有时候要自己处理回车键, 不能响应这里的预处理 }
    else if ActiveControl is TUpCnedit then
    begin
      if (ActiveControl as TUpCnedit).BKeyPreview = False then Exit
      else
      begin
        { 未特殊处理, 将回车模拟成tab键 }
        //吃掉回车键
        Key := #0;

        //发送焦点转移消息
        Perform(WM_NEXTDLGCTL, 0, 0);
      end;
    end
    else
    begin
      //未特殊处理控件发送焦点转移信号
      Key := #0;
      Perform(WM_NEXTDLGCTL, 0, 0);
    end;
  end;
end;

procedure Tfrmbase.FormCreate(Sender: TObject);
begin
  baseobj := tbase.Create;

  //来源窗体句柄
  sourceformhandle := 0;
end;

procedure Tfrmbase.FormShow(Sender: TObject);
var
  i,x,y: Integer;
begin  
  //按钮自定义左对齐
  alignleft_buttons;

  //按钮自定义右对齐
  alignright_buttons;

  { 界面控件的一些自定义处理 }
  for i:=0 to ComponentCount-1 do
  begin
    { TwwDBLookupComb的属性设置
      2013-5-16：修改输入法改变方式 imDontCare, imeName='';
    }
    if (Components[i] is TwwDBLookupCombo) then
    begin
      //设置输入法
      (Components[i] as TwwDBLookupCombo).ImeMode := imDontCare;
      (Components[i] as TwwDBLookupCombo).ImeName := '';
    end;
    if (Components[i] is TUpComboBox) then
    begin
      //设置输入法
      (Components[i] as TUpComboBox).ImeMode := imDontCare;
      (Components[i] as TUpComboBox).ImeName := '';
    end;
    if (Components[i] is TUpEdit) then
    begin
      //设置输入法
      (Components[i] as TUpEdit).ImeMode := imDontCare;
      (Components[i] as TUpEdit).ImeName := '';
    end;
    if (Components[i] is TUpDateTimePicker) then
    begin
      //设置输入法
      (Components[i] as TUpDateTimePicker).ImeMode := imDontCare;
      (Components[i] as TUpDateTimePicker).ImeName := '';
    end;
    {
    if (Components[i] is TUpCnedit) then
    begin
      //设置输入法
      (Components[i] as TUpCnedit).ImeMode := imDontCare;
      (Components[i] as TUpCnedit).ImeName := '';
    end;
    }

    { TUpWWDbGrid的一些通用的属性设置 }
    if (Components[i] is TUpWWDbGrid) then
    begin
      { TUpWWDbGrid.options += dgShowCellHint; 让其自动显示被遮盖的提示 }
      (Components[i] as TUpWWDbGrid).Options :=
          //(Components[i] as TUpWWDbGrid).Options + [dgShowCellHint,dgShowFooter];
          (Components[i] as TUpWWDbGrid).Options + [dgShowCellHint];

      //dbg画cell颜色;调用dll的函数处理
      {$IFNDEF ISAPP}
        (Components[i] as TUpWWDbGrid).OnCalcCellColors := DbgridOnCalcCellColor;
      {$ENDIF}

      //dbg选择列checkbox设置为单击选中
      (Components[i] as TUpWWDbGrid).EditControlOptions:=
        (Components[i] as TUpWWDbGrid).EditControlOptions + [ecoCheckboxsingleclick];
    end;

    { TUpPageControl, 活动页面默认切换到0页面
    }
    if (Components[i] is TUpPageControl) then
    begin
      (Components[i] as TUpPageControl).ActivePageIndex := 0;
    end;

    { TAdvDateTimePicker, timeformat格式设置 }  
    if (Components[i] is TAdvDateTimePicker) then
    begin
      (Components[i] as TAdvDateTimePicker).TimeFormat := 'HH:mm:ss';
    end;
  end;
end;

procedure Tfrmbase.alignleft_buttons;
var
  i : integer;
  btnlist : TList;
begin
  btnlist := TList.Create;

  //将按钮先按照用户设定的顺序排序
  for i := 0 to Self.ComponentCount-1 do
  begin
    if Components[i] is TUpSpeedButton then
    begin
      //是TUpSpeedButton按钮
      if (Components[i] as TUpSpeedButton).alignleftControl <> nil then
      begin
        btnlist.Add(Components[i]);
      end;
    end;
  end;

  //按钮按照用户指定的顺序排序
  btnlist.Sort(@TUpSpeedButtonCompareleft);
  //按钮按照顺序调整位置
  
  for i:=0 to btnlist.Count-1 do
  begin
    if btnlist.Items[i] <> nil then
    begin
      if TUpSpeedButton(btnlist.Items[i]).alignleftControl <> nil then
        TUpSpeedButton(btnlist.Items[i]).Left :=
            TUpSpeedButton(btnlist.Items[i]).alignleftControl.Left +
                TUpSpeedButton(btnlist.Items[i]).alignleftControl.Width + 4;
    end;
  end;

  FreeAndNil(btnlist);
end;

function TUpSpeedButtonCompareLeft(Item1, Item2: Pointer): Integer;
begin
  Result := TUpSpeedButton(Item1).alignleftorder - TUpSpeedButton(Item2).alignleftorder;
end;

function TUpSpeedButtonCompareRight(Item1, Item2: Pointer): Integer;
begin
  Result := TUpSpeedButton(Item1).alignrightorder - TUpSpeedButton(Item2).alignrightorder;
end;

procedure Tfrmbase.alignright_buttons;
var
  i : integer;
  btnlist : TList;
begin
  btnlist := TList.Create;

  //将按钮先按照用户设定的顺序排序
  for i := 0 to Self.ComponentCount-1 do
  begin
    if Components[i] is TUpSpeedButton then
    begin
      //是TUpSpeedButton按钮
      if (Components[i] as TUpSpeedButton).alignrightControl <> nil then
      begin
        btnlist.Add(Components[i]);
      end;
    end;
  end;

  //按钮按照用户指定的顺序排序
  btnlist.Sort(@TUpSpeedButtonCompareRight);
  //按钮按照顺序调整位置
     
  for i:=0 to btnlist.Count-1 do
  begin
    //最右边的按钮位置调整
    if TUpSpeedButton(btnlist.Items[i]).alignrightorder = 0 then
    begin
      TUpSpeedButton(btnlist.Items[i]).Left :=
        TUpSpeedButton(btnlist.Items[i]).Parent.Width - 8 -
          TUpSpeedButton(btnlist.Items[i]).Width;
    end
    else
    begin
      TUpSpeedButton(btnlist.Items[i]).Left :=
          TUpSpeedButton(btnlist.Items[i-1]).Left - 4 -
              TUpSpeedButton(btnlist.Items[i]).Width;
    end;
  end;
  
  {
  for i:=0 to btnlist.Count-1 do
  begin
    if btnlist.Items[i] <> nil then
    begin
      //最右边的按钮位置调整
      if TUpSpeedButton(btnlist.Items[i]).alignrightorder = 0 then
      begin
        TUpSpeedButton(btnlist.Items[i]).Left :=
          TUpSpeedButton(btnlist.Items[i]).Parent.Width - 8 -
            TUpSpeedButton(btnlist.Items[i]).Width;
      end
      else
      begin
        if TUpSpeedButton(btnlist.Items[i]).alignrightControl <> nil then
          TUpSpeedButton(btnlist.Items[i]).Left :=
              TUpSpeedButton(btnlist.Items[i]).alignrightControl.Left - 4 -
                  TUpSpeedButton(btnlist.Items[i]).Width;
      end;
    end;
  end;
  }

  FreeAndNil(btnlist);
end;

procedure Tfrmbase.FormResize(Sender: TObject);
begin     
  //按钮自定义左对齐
  alignleft_buttons;

  //按钮自定义右对齐
  alignright_buttons;
end;

procedure Tfrmbase.LoadHistory;
var
  i : Integer;
begin
  for i:=0 to ComponentCount-1 do
  begin
    //upvcl库中新增控件类型的时候要在这里增加相应的处理部分;
    
    //TUpCheckBox
    if Components[i] is TUpCheckBox then
    begin
      if (Components[i] as TUpCheckBox).HistoryValueOnOff then
        (Components[i] as TUpCheckBox).LoadHistoryValue;
    end;

    //TUpCheckListBox
    if Components[i] is TUpCheckListBox then
    begin
      if (Components[i] as TUpCheckListBox).HistoryValueOnOff then
        (Components[i] as TUpCheckListBox).LoadHistoryValue;
    end;

    //TUpCnedit
    if Components[i] is TUpCnedit then
    begin
      if (Components[i] as TUpCnedit).HistoryValueOnOff then
        (Components[i] as TUpCnedit).LoadHistoryValue;
    end;

    //TUpComboBox
    if Components[i] is TUpComboBox then
    begin
      if (Components[i] as TUpComboBox).HistoryValueOnOff then
        (Components[i] as TUpComboBox).LoadHistoryValue;
    end;
    
    //TUpDateTimePicker
    if Components[i] is TUpDateTimePicker then
    begin
      if (Components[i] as TUpDateTimePicker).HistoryValueOnOff then
        (Components[i] as TUpDateTimePicker).LoadHistoryValue;
    end;

    //TUpEdit
    if Components[i] is TUpEdit then
    begin
      if (Components[i] as TUpEdit).HistoryValueOnOff then
        (Components[i] as TUpEdit).LoadHistoryValue;
    end;

    //TUpListBox
    if Components[i] is TUpListBox then
    begin
      if (Components[i] as TUpListBox).HistoryValueOnOff then
        (Components[i] as TUpListBox).LoadHistoryValue;
    end;

    //TUpMemo
    if Components[i] is TUpMemo then
    begin
      if (Components[i] as TUpMemo).HistoryValueOnOff then
        (Components[i] as TUpMemo).LoadHistoryValue;
    end;

    //TUpRadioButton
    if Components[i] is TUpRadioButton then
    begin
      if (Components[i] as TUpRadioButton).HistoryValueOnOff then
        (Components[i] as TUpRadioButton).LoadHistoryValue;
    end;
  end;
end;

procedure Tfrmbase.SaveHistory;
var
  i : Integer;
begin
  for i:=0 to ComponentCount-1 do
  begin
    //upvcl库中新增控件类型的时候要在这里增加相应的处理部分;
    
    //TUpCheckBox
    if Components[i] is TUpCheckBox then
    begin
      if (Components[i] as TUpCheckBox).HistoryValueOnOff then
        (Components[i] as TUpCheckBox).SaveHistoryValue;
    end;

    //TUpCheckListBox
    if Components[i] is TUpCheckListBox then
    begin
      if (Components[i] as TUpCheckListBox).HistoryValueOnOff then
        (Components[i] as TUpCheckListBox).SaveHistoryValue;
    end;

    //TUpCnedit
    if Components[i] is TUpCnedit then
    begin
      if (Components[i] as TUpCnedit).HistoryValueOnOff then
        (Components[i] as TUpCnedit).SaveHistoryValue;
    end;

    //TUpComboBox
    if Components[i] is TUpComboBox then
    begin
      if (Components[i] as TUpComboBox).HistoryValueOnOff then
        (Components[i] as TUpComboBox).SaveHistoryValue;
    end;
    
    //TUpDateTimePicker
    if Components[i] is TUpDateTimePicker then
    begin
      if (Components[i] as TUpDateTimePicker).HistoryValueOnOff then
        (Components[i] as TUpDateTimePicker).SaveHistoryValue;
    end;

    //TUpEdit
    if Components[i] is TUpEdit then
    begin
      if (Components[i] as TUpEdit).HistoryValueOnOff then
        (Components[i] as TUpEdit).SaveHistoryValue;
    end;

    //TUpListBox
    if Components[i] is TUpListBox then
    begin
      if (Components[i] as TUpListBox).HistoryValueOnOff then
        (Components[i] as TUpListBox).SaveHistoryValue;
    end;

    //TUpMemo
    if Components[i] is TUpMemo then
    begin
      if (Components[i] as TUpMemo).HistoryValueOnOff then
        (Components[i] as TUpMemo).SaveHistoryValue;
    end;

    //TUpRadioButton
    if Components[i] is TUpRadioButton then
    begin
      if (Components[i] as TUpRadioButton).HistoryValueOnOff then
        (Components[i] as TUpRadioButton).SaveHistoryValue;
    end;
  end;
end;

procedure Tfrmbase.pmnbaoclsClick(Sender: TObject);
begin
  SaveHistory;
end;

procedure Tfrmbase.pmnhuiflsClick(Sender: TObject);
begin
  LoadHistory;
end;

procedure Tfrmbase.upmessageproc(var msg: TMSG);
var
  frm_w_h : Cardinal;
begin
  frm_w_h := Cardinal(msg.message);
  self.Width := frm_w_h and $0000ffff;
  self.Height := frm_w_h shr 16;
  if Self.Active then Repaint;
end;

procedure Tfrmbase.DbgridOnCalcCellColor(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //调用dll的函数处理
  {$IFNDEF ISAPP}
  CalcCellColors_main(Sender, Field, State, Highlight, AFont, ABrush);
  {$ENDIF}
end;

procedure Tfrmbase.mniqingcClick(Sender: TObject);
var
  i : integer;
begin
  { 处理右键弹出快捷菜单---清除菜单项 }
  for i:=0 to self.ComponentCount-1 do
  begin
    if (Components[i] is TWinControl) then
    begin
      if (Components[i] as TWinControl).Focused then
      begin
        //需要新的控件类型要处理，请加在这里
        //...
        
        if (Components[i] is TUpEdit) then
          (Components[i] as TUpEdit).Text := '';
                                                
        if (Components[i] is TCnSpinEdit) then
          (Components[i] as TCnSpinEdit).Text := '';
          
        if (Components[i] is TUpCnedit) then
          (Components[i] as TUpCnedit).Text := '';
          
        if (Components[i] is TUpComboBox) then
          (Components[i] as TUpComboBox).ItemIndex := -1;

        Break;
      end;
    end;
  end;   
end;

procedure Tfrmbase.nitified_sourceform;
begin
  SendMessage(sourceformhandle, UPM_DESFORM2SOURCEFORM_NOTIFIED, 0, 0);
end;

procedure Tfrmbase.upmsg_desform_notified(var msg: TMSG);
begin
  upmsg_desform_notifiedmsg_process;
end;

procedure Tfrmbase.upmsg_desform_notifiedmsg_process;
begin
  //空的实现,允许子类不处理;
end;

end.
