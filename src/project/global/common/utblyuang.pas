unit utblyuang;

interface    
uses
  SysUtils, Math, Types, DateUtils, ADODB, utypes, udbbase,
  ukeyvaluelist;

type
  {********************************************************
  *  员工表：jc_yuang
  ********************************************************}
  TTblYuang = class(TDbBase)
  public
    { 字段映射; 员工基本信息; }
    bianm : Integer;
    denglzh : string;
    xingm : string;
    xingb : string;
    jiaosbm : integer;
    jiaosmc : string;
    zhuanjbm : integer; //专家编码
    bumbm : integer; //部门信息
    bummc : string;
  public
    function getyuangxx(ibianm : Integer) : Boolean;
    //根据编码获取员工信息
    
    constructor Create(padocon : PUpAdoConnection);
    destructor Destroy(); override;
  end;
  
  { 登录员工信息
  }
  TLoginYuang = class(TTblYuang)
  end;
implementation

uses UpAdoQuery;

{ TTblYuang }

constructor TTblYuang.Create(padocon: PUpAdoConnection);
begin
  inherited Create(padocon);

end;

destructor TTblYuang.Destroy;
begin

  inherited;
end;

function TTblYuang.getyuangxx(ibianm: Integer): Boolean;
begin  
  try
    //是否是超级管理员
    if(ibianm = 0) then
    begin
      bianm := 0;
      xingm := '管理员';
      jiaosbm := 1;
      jiaosmc := '超级管理员';
                
      Result := True;
      Exit;
    end;

    //普通员工登录
    qry.OpenSql('select * from v_yuang ' +
      ' where bianm=' + IntToStr(ibianm));
    if qry.recordcount = 0 then
    begin
      qry.Close;
      raise Exception.Create('对不起，员工信息不存在!');
    end;

    bianm := qry.getInteger('bianm');
    xingm := qry.getString('xingm');
    denglzh:= qry.getString('denglzh');
    jiaosbm := qry.getInteger('jiaosbm');
    jiaosmc := qry.getString('jiaosmc');
    bumbm := qry.getInteger('bumbm');
    bummc := qry.getString('bummcqm');
    qry.Close;

    //获取登录账号映射专家编码
    qry1.OpenSql(format('select bianm from jc_zhuanj' +
      ' where denglzhbm=%d', [bianm]));
    if qry1.RecordCount>0 then
    begin
      zhuanjbm := qry1.getInteger(0);
    end
    else
    begin
      zhuanjbm := 0;
    end;
        
    Result := True;
  except
    on e:exception do
    begin
      errmsg := '获取员工信息失败:' + e.Message;
      Result := False;
    end;
  end;
end;

{ TLoginYuang }

end.
