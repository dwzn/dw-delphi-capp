
{
  导航面板顺序说明：
    0---门诊业务
    1---住院业务
    2---医技业务
    3---药库业务
    4---药房业务
    5---物资库业务
    6---财务管理
    7---院长查询
    8---数据字典
    9---基础数据
    10---系统管理
}

unit ufrmmainBG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, ExtCtrls, Buttons, UpSpeedButton, StdCtrls,
  GradientLabel, UpLabel, UPanel, RzTabs, dxGDIPlusClasses,
  umysystem, udlltransfer, udllfunctions, utypes, ufrmdbbase, frxExportXLS,
  frxClass, frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet,
  UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, jpeg;

type
  TfrmmainBG = class(Tfrmdbbase)
    wb: TWebBrowser;
    pbg: TPanel;
    imgbg: TImage;
    btnxinjps: TUpSpeedButton;
    btnpingssp: TUpSpeedButton;
    btnpingszb: TUpSpeedButton;
    btnzhuanjyj: TUpSpeedButton;
    btnjizps: TUpSpeedButton;
    btnyijls: TUpSpeedButton;
    btnxiugqr: TUpSpeedButton;
    btnpingsjs: TUpSpeedButton;
    btnguanly: TUpSpeedButton;
    btnbumld: TUpSpeedButton;
    btnpingszz: TUpSpeedButton;
    btnzhuanj: TUpSpeedButton;
    btnzuoz: TUpSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure btnxinjpsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btnxinjpsClick(Sender: TObject);
    procedure imgbgMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure btnzhuanjyjClick(Sender: TObject);
    procedure btnpingszbClick(Sender: TObject);
    procedure btnjizpsClick(Sender: TObject);
    procedure btnyijlsClick(Sender: TObject);
    procedure btnxiugqrClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    
  private
    fisok: boolean;
    function jump_piz : Boolean;
    //批注
    function jump_zhizshwd : Boolean;
    //制作上会文档
    function jump_huiyzb : Boolean;
    //会议准备
    function jump_pingsh : Boolean;
    //评审会
    function jump_wendxg : Boolean;
    //文档修改
    function jump_xiugqr : Boolean;
    //修改确认

    function getDaibsxCount(daibjd: integer): integer;
    //获取指定待办节点待办事项统计条数

    procedure setBtnDaibsxCount();
    //设置按钮待办事项标题
  protected
    function Execute_dll_show_before() : Boolean; override;

  public
    ffrmmainhandle : HWND;
    //主窗体句柄

    procedure changeNav(str : string);
    //主窗体通知切换导航面板
  end;

var
  frmmainBG: TfrmmainBG;

implementation


{$R *.dfm}

{ TfrmmainBG }

procedure TfrmmainBG.FormShow(Sender: TObject);
begin
  self.WindowState := wsMaximized;

  setBtnDaibsxCount();
end;

procedure TfrmmainBG.FormActivate(Sender: TObject);
begin
  self.WindowState := wsMaximized;

  setBtnDaibsxCount();
end;

procedure TfrmmainBG.changeNav(str: string);
begin

end;

function TfrmmainBG.Execute_dll_show_before: Boolean;
begin
  result := inherited Execute_dll_showmodal_before();

  fisok:= true;
  //设置按钮待办事项统计数字
  //setBtnDaibsxCount();

  wb.Navigate('file:///' + ExtractFilePath(Application.ExeName) + 'web/index.html');
end;

procedure TfrmmainBG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  frmmainBG := nil;
end;

procedure TfrmmainBG.FormResize(Sender: TObject);
begin
  inherited;

  pbg.Left := (self.Width-pbg.Width) div 2;
end;

procedure TfrmmainBG.btnxinjpsMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;

  { btn鼠标覆盖，变换字体颜色和样式
  }
  //(sender as TUpSpeedButton).Font.Color := clBlue;
  (sender as TUpSpeedButton).Font.Style := [fsBold]
end;

procedure TfrmmainBG.btnxinjpsClick(Sender: TObject);
//var
//  v_pform: PForm;
begin
  inherited;

  { 验证用户权限
  if not dmtbl_public.yisHasMenuitemQX(dllparams.mysystem^.loginyuang.jiaosbm,
      (sender as TUpSpeedButton).Tag) then
  begin
    baseobj.showerror('对不起，您没有此项业务权限！');
    Exit;
  end;  
  }

  { 进入业务处理
    调用frmmain窗体菜单项单击事件统一处理dll接口；
    传入菜单id以寻址对应的业务处理函数；
    菜单id在配置sql中已经分配给定；菜单项的显示顺序由listorder字段单独控制；
  }
  {
  v_pform:= frmmain_menuitem_click(@dllparams, (sender as TUpSpeedButton).Tag);
  dllparams.mysystem^.FAppendChildForm(v_pform^);

  //业务窗体启动后关闭导航栏
  SendMessage(ffrmmainhandle, UPM_FRMMAIN_LEFTNAVBAR_CLOSE, 0, 0);
  }
end;

procedure TfrmmainBG.imgbgMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  i : integer;
begin
  { 模拟鼠标在btn上的离开事件
    按钮字体颜色样式恢复正常
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    if self.Components[i] is TUpSpeedButton then
    begin
      {
      (self.Components[i] as TUpSpeedButton).Font.Color := clBlack;
      }
      (self.Components[i] as TUpSpeedButton).Font.Style := [];
    end;
  end;
end;


function TfrmmainBG.jump_pingsh: Boolean;
begin
  {
  if not assigned(frmpingslist_pingsh) then
    frmpingslist_pingsh := tfrmpingslist_pingsh.Create(nil);
  frmpingslist_pingsh.Execute_dll_show(@dllparams);
  dllparams.mysystem^.FAppendChildForm(frmpingslist_pingsh);
  }
end;


procedure TfrmmainBG.btnzhuanjyjClick(Sender: TObject);
begin
  inherited;

  jump_piz();
end;

procedure TfrmmainBG.btnpingszbClick(Sender: TObject);
begin
  inherited;

  jump_zhizshwd();
end;

procedure TfrmmainBG.btnjizpsClick(Sender: TObject);
begin
  inherited;

  jump_pingsh();
end;

procedure TfrmmainBG.btnyijlsClick(Sender: TObject);
begin
  inherited;

  jump_wendxg();
end;

procedure TfrmmainBG.btnxiugqrClick(Sender: TObject);
begin
  inherited;

  jump_xiugqr();
end;

function TfrmmainBG.getDaibsxCount(daibjd: integer): integer;
begin
  qry1.OpenSql(format('select count(*) from ls_pings_daib' +
    ' where yuangbm=%d and daibjd=%d' +
    '   and zhuangt=0', [dllparams.mysystem^.loginyuang.bianm, daibjd]));

  Result:= qry1.getInteger(0);
end;

procedure TfrmmainBG.setBtnDaibsxCount;
var
  icount: integer;
begin
  if not fisok then exit;

  //设置界面通知类信息
end;

procedure TfrmmainBG.FormCreate(Sender: TObject);
begin
  inherited;

  fisok:= false;
end;

function TfrmmainBG.jump_huiyzb: Boolean;
begin

end;

function TfrmmainBG.jump_piz: Boolean;
begin

end;

function TfrmmainBG.jump_wendxg: Boolean;
begin

end;

function TfrmmainBG.jump_xiugqr: Boolean;
begin

end;

function TfrmmainBG.jump_zhizshwd: Boolean;
begin

end;

end.
