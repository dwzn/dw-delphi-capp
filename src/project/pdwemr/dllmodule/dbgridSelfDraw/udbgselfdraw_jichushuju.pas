unit udbgselfdraw_jichushuju;

interface

uses
  SysUtils,
  DB,
  Dialogs,
  Graphics,
  ADODB,
  Grids,
  Messages,
  Windows,
  Wwdbigrd,
  Wwdbgrid,
  UpWWDbGrid,
  udbgselfdraw_dbgid_define;

//基础数据---床位管理
procedure dbgCalcCellColor_jicsj_chuangw(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
//基础数据---项目设置
procedure dbgCalcCellColor_jicsj_xiangm(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
const dbgSelfDrawIID_jicsj_chuangw = 'dbgSelfDrawIID_jicsj_chuangw';
const dbgSelfDrawIID_jicsj_xiangm = 'dbgSelfDrawIID_jicsj_xiangm';


implementation

//基础数据---床位管理
procedure dbgCalcCellColor_jicsj_chuangw(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //床位状态调整颜色
  case Field.DataSet.FieldByName('zhuangt').AsInteger of
    0: //空闲床位
    begin
      ABrush.Color := StringToColor(dbg_edit_color);
    end;
    1: //正在使用中
    begin
      ABrush.Color := StringToColor(dbg_readonly_color);
    end;
    2: //停止使用
    begin
      ABrush.Color := StringToColor(dbg_readonly_color);
    end;
  end;
end;

//基础数据---项目设置
procedure dbgCalcCellColor_jicsj_xiangm(Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  //项目状态
  case Field.DataSet.FieldByName('zhuangt').AsInteger of
    0: //项目停用
    begin               
      AFont.Color := clRed;
      ABrush.Color := StringToColor(dbg_readonly_color);
    end;
    1: //项目正常
    begin
      ABrush.Color := StringToColor(dbg_edit_color);
    end;
  end;
end;

end.
