unit ufrmcxbb_xitgl_jiekrz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmcxbb1DbgridbaseMDI, ImgList, frxExportXLS, frxClass,
  frxExportPDF, Menus, UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB,
  ADODB, UpAdoTable, UpAdoQuery, Buttons, UpSpeedButton, ExtCtrls,
  ToolPanels, UpAdvToolPanel, Grids, Wwdbigrd, Wwdbgrid, UpWWDbGrid,
  RzButton, RzPanel, StdCtrls, UpLabel, UpComboBox;

type
  Tfrmcxbb_xitgl_jiekrz = class(Tfrmcxbb1DbgridbaseMDI)
    cbbjiek: TUpComboBox;
    lbl1: TUpLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    function BuildSqlString() : Boolean; override;
    function PrintBefore() : Boolean; override;
    
    function Execute_dll_show_before() : Boolean; override;
  end;

var
  frmcxbb_xitgl_jiekrz: Tfrmcxbb_xitgl_jiekrz;

implementation

uses ufrmdbbase;

{$R *.dfm}

function Tfrmcxbb_xitgl_jiekrz.BuildSqlString: Boolean;
var
  strsql: string;
begin
  Result:= false;

  strsql:= ' where 1=1';

  //接口
  if cbbjiek.ItemIndex<>-1 then
  begin
    strsql:= strsql + format(' and jiekbm=%s', [cbbjiek.GetItemValue()]);
  end;

  qryreport_m.SQL.Text:= 'select * from v_sys_intf_riz ' + strsql;

  Result:= True;
end;

function Tfrmcxbb_xitgl_jiekrz.Execute_dll_show_before: Boolean;
begin
  Result:= inherited Execute_dll_show_before();

  { 查询面板初始化
  }
  //接口
  dmtbl_public.fillcbb_userTableName(@cbbjiek, 'sys_intf');
end;

procedure Tfrmcxbb_xitgl_jiekrz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  frmcxbb_xitgl_jiekrz:= nil;
end;

function Tfrmcxbb_xitgl_jiekrz.PrintBefore: Boolean;
begin
  Result:= inherited PrintBefore();
end;

end.
